package com.example.asee_gps_pokepin.data.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import com.example.asee_gps_pokepin.models.Comment;
import com.example.asee_gps_pokepin.models.Event;
import com.example.asee_gps_pokepin.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class EventRepositoryTest {

    private EventRepository eventRepositoryUnderTest;
    private String email = "test@test.com";

    @Before
    public void setUp() {
        String password = "123456";
        String name = "Name";
        String surname = "Surname";

        UserRepository userRepository = Mockito.mock(UserRepository.class);
        userRepository.createUserWithEmailAndPassword(email, password, new User(name, surname, email));

        eventRepositoryUnderTest = Mockito.mock(EventRepository.class);
    }

    @Test
    public void testGetEvents_And_InsertEvent() {
        // Setup
        String name = "testEvent";
        String description = "Description of the testEvent";
        Event event = new Event(0.0, 0.0, email, name, description);
        Map<String,Event> eventMap = new HashMap<>();
        eventMap.put(name, event);

        eventRepositoryUnderTest.insertEvent(0.0, 0.0, name, description);


        // Run the test

        when(eventRepositoryUnderTest.getEvents()).thenReturn(new MutableLiveData<>(eventMap));

        // Verify the results
        assertEquals(eventRepositoryUnderTest.getEvents().getValue(), eventMap);
    }

    @Test
    public void testRate() {
        // Setup
        String name = "testEvent";
        String description = "Description of the testEvent";
        Event event = new Event(0.0, 0.0, email, name, description);Map<String,Event> eventMap = new HashMap<>();
        ArrayList<Float> rats = new ArrayList<>();
        rats.add(0.0f);
        event.setRatings(rats);
        eventMap.put(name, event);

        // Run the test
        eventRepositoryUnderTest.rate(0.0f, "selectedKey");
        when(eventRepositoryUnderTest.getEvents()).thenReturn(new MutableLiveData<>(eventMap));

        // Verify the results
        assertEquals(event.calculateAverageRating(), Objects.requireNonNull(Objects.requireNonNull(eventRepositoryUnderTest.getEvents().getValue()).get(name)).calculateAverageRating(), 0.00001);
    }

    @Test
    public void testUpdateEvent() {
        // Setup
        String name = "testEvent";
        String description = "Description of the testEvent";
        Event event = new Event(0.0, 0.0, email, name, description);
        Map<String,Event> eventMap = new HashMap<>();
        eventMap.put(name, event);
        eventMap.replace(name, new Event(2.0, 2.2, email, "name", "Description 2"));

        // Run the test

        eventRepositoryUnderTest.updateEvent("key", 2.0, 2.2, "name", "description");
        when(eventRepositoryUnderTest.getEvents()).thenReturn(new MutableLiveData<>(eventMap));

        // Verify the results

        assertEquals(eventRepositoryUnderTest.getEvents().getValue(), eventMap);
    }

    @Test
    public void testDeleteEvent() {
        // Setup
        String name = "testEvent";
        String description = "Description of the testEvent";
        Event event = new Event(0.0, 0.0, email, name, description);
        Map<String,Event> eventMap = new HashMap<>();
        eventMap.put(name, event);

        eventRepositoryUnderTest.insertEvent(0.0, 0.0, name, description);

        // Run the test

        eventRepositoryUnderTest.deleteEvent(name);
        when(eventRepositoryUnderTest.getEvents()).thenReturn(new MutableLiveData<>(eventMap));

        // Verify the results
        assertEquals(eventRepositoryUnderTest.getEvents().getValue(), eventMap);
    }

    @Test
    public void testGetCommentsEvent_And_SendComment() {
        // Setup
        String key = "eventTest";
        String message = "commentTest";
        Comment testComment = new Comment(email, message);
        LiveData<List<Comment>> commentList = new MutableLiveData<>(new ArrayList<>());
        Objects.requireNonNull(commentList.getValue()).add(testComment);

        eventRepositoryUnderTest.sendComment(message, key);

        // Run the test
        when(eventRepositoryUnderTest.getCommentsEvent(key)).thenReturn(commentList);

        // Verify the results
        //when().thenReturn(commentList);
        assertEquals(eventRepositoryUnderTest.getCommentsEvent(key).getValue(), commentList.getValue());

    }
}
