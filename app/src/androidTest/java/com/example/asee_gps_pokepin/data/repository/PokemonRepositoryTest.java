package com.example.asee_gps_pokepin.data.repository;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import com.example.asee_gps_pokepin.LiveDataTestUtils;
import com.example.asee_gps_pokepin.models.Pokemon;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(AndroidJUnit4.class)
@SmallTest
public class PokemonRepositoryTest {

    private PokemonRepository pokemonRepositoryUnderTest;

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        pokemonRepositoryUnderTest = PokemonRepository.getInstance(ApplicationProvider.getApplicationContext());
        pokemonRepositoryUnderTest.loadAllPokemon();
    }



    @Test
    public void testSearchByName() throws InterruptedException {
        // Setup

        // Run the test
        LiveData<List<Pokemon>> result = pokemonRepositoryUnderTest.searchByName("Mew");
        List<Pokemon> pok = LiveDataTestUtils.getValue(result);

        boolean funciona = true;
        for (Pokemon p : pok )
            if (!(p.getName().toLowerCase().equals("mewtwo")||p.getName().toLowerCase().equals("mew")))
                funciona=false;

        // Verify the results
        assertTrue(pok.size() == 2);
        assertTrue(funciona);
    }

    @Test
    public void testSearchSingleByName() throws InterruptedException {
        // Setup

        // Run the test
        final LiveData<Pokemon> result = pokemonRepositoryUnderTest.searchSingleByName("Mew");
        Pokemon pok = LiveDataTestUtils.getValue(result);

        // Verify the results
        assertTrue(pok.getName().toLowerCase().equals("mew"));
    }


    @Test
    public void testGetPokemonInfo() throws InterruptedException {
        // Setup
        LiveData<List<Pokemon>> livePokemon = pokemonRepositoryUnderTest.getAllPokemon();
        List<Pokemon> pok = LiveDataTestUtils.getValue(livePokemon);

        // Run the test
        int pokeId = 1;
        Pokemon p = pok.get(pokeId-1);

        // Verify the results
        assertEquals( "bulbasaur", p.getName());
        assertEquals(pokeId, (int)p.getId());
    }
}