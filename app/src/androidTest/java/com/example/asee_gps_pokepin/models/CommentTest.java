package com.example.asee_gps_pokepin.models;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class CommentTest {

    private Comment commentUnderTest;

    @Before
    public void setUp() {
        commentUnderTest = new Comment("user", "comment");
    }

    @Test
    public void testGetUser() {
        assertEquals("user", commentUnderTest.getUser());
    }

    @Test
    public void testSetUser() {
        commentUnderTest.setUser("test_user");
        assertEquals("test_user", commentUnderTest.getUser());
    }

    @Test
    public void testGetComment() {
        assertEquals("comment", commentUnderTest.getComment());
    }
}
