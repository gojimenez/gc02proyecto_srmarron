package com.example.asee_gps_pokepin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.asee_gps_pokepin.auth.UserViewModel;
import com.example.asee_gps_pokepin.models.User;
import com.example.asee_gps_pokepin.ui.NavigationDrawerActivity;

import java.util.Objects;

public class SignUpActivity extends AppCompatActivity {

    private EditText nameEditText, surnameEditText, emailEditText, passwordEditText, repeatpasswordEditText;
    private UserViewModel userViewModel;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.activity_sign_up);

        nameEditText = findViewById(R.id.signup_name);
        surnameEditText = findViewById(R.id.signup_surname);
        emailEditText = findViewById(R.id.signup_email);
        passwordEditText = findViewById(R.id.signup_password);
        repeatpasswordEditText = findViewById(R.id.signup_repeatpassword);
        Button signButton = findViewById(R.id.signup_complete);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);



        signButton.setOnClickListener(v -> {

            final String name =  nameEditText.getText().toString().trim();
            final String surname = surnameEditText.getText().toString().trim();
            final String email = emailEditText.getText().toString().trim();
            final String password = passwordEditText.getText().toString().trim();
            final String repeatPassword = repeatpasswordEditText.getText().toString().trim();

            if (TextUtils.isEmpty(name)) {
                Toast.makeText(getApplicationContext(), "Please, enter a name", Toast.LENGTH_SHORT).show();
                return;
            }

            if (TextUtils.isEmpty(surname)) {
                Toast.makeText(getApplicationContext(), "Please, enter a surname", Toast.LENGTH_SHORT).show();
                return;
            }

            if (TextUtils.isEmpty(email)) {
                Toast.makeText(getApplicationContext(), "Please, enter email address", Toast.LENGTH_SHORT).show();
                return;
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(getApplicationContext(), "PleaseEnter password", Toast.LENGTH_SHORT).show();
                return;
            }

            if (password.length() < 6) {
                Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!password.equals(repeatPassword)) {
                Toast.makeText(getApplicationContext(), "Password not matching, check both passwords", Toast.LENGTH_SHORT).show();
                return;
            }

            User u = new User(name, surname, email);

            //create user
            if (userViewModel.createUserWithEmailAndPassword(email, password, u)){
                saveValue("user",email);
                saveValue("password",password);

                finishAffinity();

                startActivity(new Intent(SignUpActivity.this, NavigationDrawerActivity.class));
            }
        });
    }
    public void saveValue(String keyPref, String valor) {
        SharedPreferences settings = getSharedPreferences("userdetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putString(keyPref, valor);
        editor.apply();
    }

}
