package com.example.asee_gps_pokepin.ui.favs.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.asee_gps_pokepin.ui.favs.ListFavsAdapter;
import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.ui.favs.viewmodel.PokemonFavViewModel;

import java.util.Objects;


public class FavsFragment extends Fragment {

    private ListFavsAdapter listFavsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_favs, container, false);
        RecyclerView recyclerView = root.findViewById(R.id.recyclerViewFavs);


        listFavsAdapter = new ListFavsAdapter(this.getContext());
        recyclerView.setAdapter(listFavsAdapter);
        recyclerView.setHasFixedSize(true);
        final GridLayoutManager layoutManager = new GridLayoutManager(null, 2);
        recyclerView.setLayoutManager(layoutManager);
        PokemonFavViewModel pokemonFavViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(PokemonFavViewModel.class);
        pokemonFavViewModel.getPokemonFav().observe(getActivity(), pokemon -> listFavsAdapter.addListaPokemon(pokemon));
        return root;
    }
}
