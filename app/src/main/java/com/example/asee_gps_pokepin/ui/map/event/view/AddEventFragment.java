package com.example.asee_gps_pokepin.ui.map.event.view;


import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.ui.map.event.viewmodel.EventViewModel;

import java.io.IOException;
import java.util.List;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddEventFragment extends Fragment {

    private EventViewModel eventViewModel;

    private EditText event_name, event_description, event_address;


    public AddEventFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_event, container, false);

        eventViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(EventViewModel.class);

        event_name = root.findViewById(R.id.add_event_name_value);
        event_description = root.findViewById(R.id.add_event_description_value);
        event_address = root.findViewById(R.id.add_event_address_value);

        Button submit = root.findViewById(R.id.add_event_submit);
        submit.setOnClickListener(view -> {
            String eName, eDescription, eAddress;

            eName = event_name.getText().toString();
            eDescription = event_description.getText().toString();
            eAddress = event_address.getText().toString();

            if (eName.isEmpty()){
                Toast.makeText(root.getContext(), "Please enter a name.", Toast.LENGTH_SHORT).show();
            }else{

                if (!eAddress.isEmpty()) {
                    Geocoder geo = new Geocoder(root.getContext());
                    List<Address> addresses = null;
                    try {
                        addresses = geo.getFromLocationName(eAddress, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (addresses != null) {
                        if (addresses.size() > 0) {
                            eventViewModel.insertEvent(addresses.get(0).getLatitude(), addresses.get(0).getLongitude(), eName, eDescription);
                            Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigateUp();
                        } else
                            Toast.makeText(root.getContext(), "Please enter an address", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        return root;
    }

}
