package com.example.asee_gps_pokepin.ui.map.event.comment;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.asee_gps_pokepin.R;

public class CommentsHolder extends RecyclerView.ViewHolder {

    private TextView user;
    private TextView comment;

    CommentsHolder(@NonNull View itemView) {
        super(itemView);
        user = itemView.findViewById(R.id.comment_user);
        comment = itemView.findViewById(R.id.comment_content);
    }

    public TextView getUser() {
        return user;
    }

    public void setUser(TextView user) {
        this.user = user;
    }

    TextView getComment() {
        return comment;
    }

}
