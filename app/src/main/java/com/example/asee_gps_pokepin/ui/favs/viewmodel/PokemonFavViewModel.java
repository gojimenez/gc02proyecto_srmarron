package com.example.asee_gps_pokepin.ui.favs.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.asee_gps_pokepin.data.repository.PokemonFavRepository;
import com.example.asee_gps_pokepin.models.Pokemon;

import java.util.List;

public class PokemonFavViewModel extends AndroidViewModel {
    private PokemonFavRepository pokemonFavRepository;


    public PokemonFavViewModel(@NonNull Application application) {
        super(application);

        pokemonFavRepository = PokemonFavRepository.getInstance(application);
    }

    public LiveData<List<Pokemon>> getPokemonFav() {

        return pokemonFavRepository.getPokemonFav();
    }

    public LiveData<Boolean> isPokemonFav(Integer idPokemon){
        return pokemonFavRepository.isFavourite(idPokemon);
    }

    public void switchFavourite (Integer idPokemon) {
        pokemonFavRepository.switchFavourite(idPokemon);
    }
}
