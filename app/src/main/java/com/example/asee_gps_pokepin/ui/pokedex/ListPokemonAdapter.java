package com.example.asee_gps_pokepin.ui.pokedex;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.Pokemon;
import com.example.asee_gps_pokepin.ui.pokedex.viewmodel.PokedexViewModel;

import java.util.ArrayList;
import java.util.List;

public class ListPokemonAdapter extends RecyclerView.Adapter<ListPokemonAdapter.ViewHolder> {

    private ArrayList<Pokemon> dataset;
    private Context context;

    private PokedexViewModel pokedexViewModel;

    public ListPokemonAdapter(Context context, PokedexViewModel pokedexViewModel) {
        this.context = context;
        dataset = new ArrayList<>();
        this.pokedexViewModel = pokedexViewModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Pokemon p = dataset.get(position);
        holder.nombreTextView.setText(p.getName());

        if(p.getId()!= null)
            Glide.with(context)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+ (p.getId())+ ".png")
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.fotoImageView);
        else
            Glide.with(context)
                    .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+ (position+1)+ ".png")
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.fotoImageView);

        holder.fotoImageView.setOnClickListener(view -> {
            pokedexViewModel.select(p);
            Navigation.findNavController((Activity)context, R.id.nav_host_fragment).navigate(R.id.pokemon_info_fragment);
        });

    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void addListaPokemon (List<Pokemon> listaPokemon) {
        dataset.clear();
        dataset.addAll(listaPokemon);
        notifyDataSetChanged();
    }



    public ArrayList<Pokemon> getPokemon(){
        return dataset;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView fotoImageView;
        private TextView nombreTextView;

        ViewHolder(View itemView) {
            super(itemView);

            fotoImageView = itemView.findViewById(R.id.fotoImageView);
            nombreTextView = itemView.findViewById(R.id.nombreTextView);
        }
    }


    @NonNull
    @Override
    public String toString() {
        StringBuilder total= new StringBuilder();
        for (Pokemon p:dataset)
            total.append(p.toString());
        return total.toString();
    }
}
