package com.example.asee_gps_pokepin.ui.map.event.view;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.asee_gps_pokepin.R;
import com.example.asee_gps_pokepin.models.User;
import com.example.asee_gps_pokepin.ui.map.event.viewmodel.EventViewModel;
import com.example.asee_gps_pokepin.auth.UserViewModel;

import java.io.IOException;
import java.util.List;
import java.util.Objects;


public class ShowEventFragment extends Fragment {

    private EventViewModel eventViewModel;
    private TextView event_name, event_address, event_email, event_description, event_ratingNumber;

    private RatingBar event_ratingBar;

    private Button event_editEvent;

    private Button event_deleteEvent;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        eventViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(EventViewModel.class);

        View root = inflater.inflate(R.layout.fragment_event_info, container, false);

        UserViewModel userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);

        event_name = root.findViewById(R.id.event_name_value);
        event_address = root.findViewById(R.id.event_address_value);
        event_email = root.findViewById(R.id.event_email_value);
        event_description = root.findViewById(R.id.event_description_value);
        event_ratingBar = root.findViewById(R.id.event_rating);
        event_ratingNumber = root.findViewById(R.id.event_rating_number);
        Button event_showComments = root.findViewById(R.id.event_show_comments);
        event_editEvent = root.findViewById(R.id.edit_event);
        event_deleteEvent = root.findViewById(R.id.delete_event);

        event_showComments.setOnClickListener(v -> Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigate(R.id.show_event_comments_fragment));

        event_editEvent.setOnClickListener(v -> Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.edit_event_fragment));

        event_deleteEvent.setOnClickListener(view -> delete());

        eventViewModel.getEvents().observe(this, stringEventMap -> stringEventMap.forEach((k,event)-> {
            if(event.getName().equals(Objects.requireNonNull(eventViewModel.getSelected().getValue()).getName())
                    && event.getGeoLat() == eventViewModel.getSelected().getValue().getGeoLat()
                    && event.getGeoLong() == eventViewModel.getSelected().getValue().getGeoLong()) {
                eventViewModel.setSelectedKey(k);
            }
        }));

        event_ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            event_ratingNumber.setText(String.valueOf(rating));
            eventViewModel.rate(rating);
        });

        userViewModel.getCurrentUser().observe(this, this::displayEditDelete);

        eventViewModel.getSelected().observe(this, event -> {
            Geocoder geo = new Geocoder(getContext());
            List<Address> addresses;
            try {
                addresses = geo.getFromLocation(event.getGeoLat(),event.getGeoLong(),1);
                event_address.setText(addresses.get(0).getAddressLine(0));
                event_name.setText(event.getName());
                event_email.setText(event.getUserEmail());
                event_description.setText(event.getDescription());
                event_ratingBar.setRating(event.calculateAverageRating());
                event_ratingNumber.setText(String.valueOf(event.calculateAverageRating()));

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return root;
    }

    private void delete() {
        eventViewModel.getSelectedKey().observe(this, s -> {
            eventViewModel.deleteEvent(s);
            Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigateUp();
        });
    }

    private void displayEditDelete(User u) {
        if (u.getEmail().equals(Objects.requireNonNull(eventViewModel.getSelected().getValue()).getUserEmail())) {
            event_editEvent.setVisibility(View.VISIBLE);
            event_deleteEvent.setVisibility(View.VISIBLE);
        }
    }


}
