package com.example.asee_gps_pokepin.data.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.asee_gps_pokepin.models.Comment;
import com.example.asee_gps_pokepin.models.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class EventRepository {

    private DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Events");

    private MutableLiveData<Map<String, Event>> events = new MutableLiveData<>();

    private MutableLiveData<List<Comment>> commentsEvent = new MutableLiveData<>();

    private static EventRepository INSTANCE;

    public static EventRepository getInstance() {
        if (INSTANCE == null)
            INSTANCE = new EventRepository();
        return INSTANCE;
    }

    public LiveData<Map<String,Event>> getEvents () {
        Map <String,Event> mapEvents = new HashMap<>();
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Event e = ds.getValue(Event.class);
                        assert e != null;
                        getRatings(e, ds.getKey());
                        mapEvents.put(ds.getKey(), e);
                    }
                    events.postValue(mapEvents);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });


        return events;
    }

    private void getRatings(Event e, String key){
        DatabaseReference refAux = ref.child(key).child("Ratings");

        final ArrayList<Float> rats = new ArrayList<>();
        refAux.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    rats.clear();
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        float rat = Float.parseFloat(Objects.requireNonNull(ds.getValue()).toString());
                        rats.add(rat);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        e.setRatings(rats);
    }

    public void rate(float rating, String selectedKey){
        ref.child(selectedKey).child("Ratings").child(Objects.requireNonNull(UserRepository.getInstance().getCurrentUserUid().getValue())).setValue(rating);
    }

    public void sendComment (String message, String selectedKey) {
        Comment comment = new Comment(Objects.requireNonNull(UserRepository.getInstance().getCurrentUser().getValue()).getEmail(), message);
        ref.child(selectedKey).child("Comments").push().setValue(comment);
    }

    public void insertEvent(double geoLat, double geoLong, String name, String description) {
        Event event = new Event(geoLat, geoLong, Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getEmail(), name, description);
        ref.push().setValue(event);
    }

    public void updateEvent(String key, double geoLat, double geoLong, String name, String description) {
        Event event = new Event(geoLat, geoLong, Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getEmail(), name, description);
        ref.child(key).setValue(event);
    }

    public void deleteEvent(String key) {
        ref.child(key).removeValue();
    }

    public LiveData<List<Comment>> getCommentsEvent(String key) {
        List<Comment> comments = new ArrayList<>();
        if (commentsEvent.getValue() != null)
            commentsEvent.getValue().clear();
        ref.child(key).child("Comments").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Comment c = dataSnapshot.getValue(Comment.class);
                comments.add(c);
                commentsEvent.postValue(comments);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return commentsEvent;
    }
}
