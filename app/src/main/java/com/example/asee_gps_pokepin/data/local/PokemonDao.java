package com.example.asee_gps_pokepin.data.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.asee_gps_pokepin.models.Pokemon;

import java.util.List;

@Dao
public interface PokemonDao {

    @Query("SELECT * FROM Pokemon")
    LiveData<List<Pokemon>> getAll();

    @Query("SELECT * FROM Pokemon " +
            "WHERE Pokemon.name LIKE '%' || :s || '%'")
    LiveData<List<Pokemon>> getAllByName(String s);

    @Query("SELECT * FROM Pokemon " +
            "WHERE Pokemon.name LIKE :s")
    LiveData<Pokemon> getByName(String s);

    @Query("SELECT * FROM Pokemon " +
            "WHERE Pokemon.id LIKE :id")
    LiveData<Pokemon> getPokemon(int id);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Pokemon pokemon);

}
