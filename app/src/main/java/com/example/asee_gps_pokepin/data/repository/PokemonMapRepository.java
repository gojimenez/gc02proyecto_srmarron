package com.example.asee_gps_pokepin.data.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.asee_gps_pokepin.models.PokemonMap;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class PokemonMapRepository {

    private DatabaseReference ref = FirebaseDatabase.getInstance().getReference("pokemonsMap");;

    private MutableLiveData<Map<String,PokemonMap>> pokemonsMap = new MutableLiveData<>();

    private static PokemonMapRepository INSTANCE;

    public static synchronized PokemonMapRepository getInstance() {
        if (INSTANCE == null)
            INSTANCE = new PokemonMapRepository();
        return INSTANCE;
    }


    public LiveData<Map<String, PokemonMap>> getPokemonsMap() {
        Map <String,PokemonMap> mapPokemonsMap = new HashMap<>();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        PokemonMap pokemonMap = ds.getValue(PokemonMap.class);
                        if (pokemonMap != null)
                            mapPokemonsMap.put(ds.getKey(), pokemonMap);
                    }
                    pokemonsMap.postValue(mapPokemonsMap);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return pokemonsMap;
    }

    public void insertPokemonMap (PokemonMap pokemonMap) {
        ref.push().setValue(pokemonMap);
    }

}
